#!/bin/bash
set -e
 
# instalar apache2 y git
 
apt update && apt install apache2 git -y
 
 
# clonar repositorio via https
 
git clone https://gitlab.com/gudpozzo/docker.git /docker

cp /docker/index.html /var/www/html

exec $@
