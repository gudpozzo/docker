# Proyecto Docker con Apache y Bootstrap

## Descripción

Este proyecto tiene como objetivo crear y ejecutar un contenedor Docker que va a correr un sitio web utilizando Apache y Bootstrap.

## Pasos para crear y ejecutar un contenedor Docker

### 1. Descargarmos los archivos necesarios

Primero, descargar los ejemplos de Bootstrap:

```sh
cd ~/docker
wget https://github.com/twbs/bootstrap/releases/download/v5.3.2/bootstrap-5.3.2-examples.zip
```

### 2. Instalamos `unzip` y descomprimimos el archivo

Instalar `unzip` si no lo tenes:

```sh
sudo apt install unzip
```

Descomprimimos el archivo descargado:

```sh
unzip bootstrap-5.3.2-examples.zip
```

### 3. Crear el Dockerfile

procedemos a crear un archivo con el nombre `Dockerfile` con el siguiente contenido:

```Dockerfile
# Dockerfile
FROM debian:latest

COPY entrypoint.sh /entrypoint.sh

WORKDIR /var/www/html

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

EXPOSE 80

CMD ["apache2ctl", "-D", "FOREGROUND"]
```

### 4. Crear el archivo `entrypoint.sh`

procedemos a crar un archivo con el siguiente nombre `entrypoint.sh` y el siguiente contenido:

```sh
#!/bin/bash
set -e

# Instalar apache2 y git
apt update && apt install apache2 git -y

# Clonar repositorio via https
git clone https://gitlab.com/gudpozzo/docker.git /docker

# Copiar el archivo index.html al directorio web
cp /docker/index.html /var/www/html

exec "$@"
```

hacemos el archivo ejecutable:

```sh
chmod +x entrypoint.sh
```

### 5. Crear el archivo `index.html`

Editar o crear el archivo `index.html` a gusto:

```sh
vim index.html
```

### 6. Sincronización con GitLab

Añadimos y comentamos nuestro cambios:

```sh
git add .
git commit -m "Se realizaron los cambios en la configuración de entrypoint y se agrega index.html"
```

procedemos a hacer un push de los cambios realizados en nuestro repositorio GitLab:

```sh
git push origin main
```

### 7. Construimos la imagen Docker

Compilamos la imagen Docker de la siguiente manera:

```sh
docker build -t apache2:prueba4 .
```

### 8. Lanzar el contenedor

Iniciamos un contenedor a partir de la imagen creada:

```sh
docker run -d --name apache2 -p 8080:80 apache2:prueba4
```

Iniciamos otro contenedor para pruebas adicionales:

```sh
docker run -d --name apache2-prueba2 -p 8081:80 apache2:prueba4
```

### 9. Verificación

Verificamos que los contenedores estén funcionando correctamente de la siguiente manera:

```sh
docker ps
```


